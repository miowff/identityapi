﻿using IdentityApi.Core.Models;
using IdentityApi.Core.ServicesInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace IdentityApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        public UsersController(IUsersService usersService)
        {
            this._usersService = usersService;
        }
        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> RegistrationAction([FromBody] UserRegistrationRequestModel registrationRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _usersService.Register(registrationRequest);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Invalid model state");
        }
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> LoginAction([FromBody] UserLoginRequestModel userLoginRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _usersService.Login(userLoginRequest);
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Invalid model state");
        }
        [HttpPost]
        [Route("RefreshToken")]
        public async Task<IActionResult> RefreshToken([FromBody] TokenRequest tokenRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await this._usersService.RefreshToken(tokenRequest);
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Invalid model state");
        }
       
    }
}
