﻿using IdentityApi.Core.Models.ConfirmEmailModels;
using IdentityApi.Core.ServicesInterfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IdentityApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EmailsController : ControllerBase
    {
        private readonly IEmailService _emailService;
        public EmailsController(IEmailService emailService)
        {
            this._emailService = emailService;
        }
        [HttpPost]
        [Route("GetConfirmationCode")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetConfirmationCode([FromBody]GetCodeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await this._emailService.SendConfirmationCode(model.identityUserId);
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Invalid model state");
        }
        [HttpPost]
        [Route("ConfirmEmail")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> ConfirmEmail([FromBody]ConfirmEmailModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await this._emailService.ConfirmEmail(model.identityUserId, model.code);
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Invalid model state");
        }
    }
}
