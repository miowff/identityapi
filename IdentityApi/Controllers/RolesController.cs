﻿using IdentityApi.Core.Models;
using IdentityApi.Core.ServicesInterfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IdentityApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RolesController:ControllerBase
    {
        private readonly IRolesService _rolesService;
        public RolesController(IRolesService rolesService)
        {
            this._rolesService = rolesService;
        }
        [HttpPost]
        [Route("CreateNewRole")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateRole(string roleName)
        {
            try
            {
                await _rolesService.CreateRole(roleName);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteRole")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteRole(string roleName)
        {
            try
            {
                await _rolesService.DeleteRole(roleName);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        [Route("ChangeRoleName")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateRoleName([FromBody] UpdateRoleNameModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _rolesService.UpdateRoleName(model);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Invalid model state!");
        }
    }
}
