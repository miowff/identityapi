﻿
namespace IdentityApi.Core.ServicesInterfaces
{
    public interface IEmailService
    {
        Task<bool> SendConfirmationCode(string identityUserId);
        Task<bool> ConfirmEmail(string identityUserId, string code);
    }
}
