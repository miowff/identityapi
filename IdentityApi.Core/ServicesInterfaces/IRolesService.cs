﻿using IdentityApi.Core.Models;

namespace IdentityApi.Core.ServicesInterfaces
{
    public interface IRolesService
    {
        Task CreateRole(string roleName);
        Task DeleteRole(string roleName);
        Task UpdateRoleName(UpdateRoleNameModel model);
    }
}
