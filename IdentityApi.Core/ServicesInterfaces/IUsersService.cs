﻿
using IdentityApi.Core.Models;

namespace IdentityApi.Core.ServicesInterfaces
{
    public interface IUsersService
    {
        Task DeleteAsync(string email);
        Task UpdateAsync(UpdateUserRequestModel model, string oldEmail);
        Task ChangePasswordAsync(ChangePasswordRequestModel changePasswordModel);
        Task Register(UserRegistrationRequestModel model);
        Task<UserLoginResponceModel> Login(UserLoginRequestModel model);
        Task<UserLoginResponceModel> RefreshToken(TokenRequest tokenRequest);
    }
}
