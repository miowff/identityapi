﻿using IdentityApi.Core.Models;
using System.Security.Claims;
namespace IdentityApi.Core.ServicesInterfaces
{
    public interface IJwtTokenService
    {
        Task<UserLoginResponceModel> GenerateToken(List<Claim> claims);
        ClaimsPrincipal GetClaimsPrincipalToken(string token);
    }
}
