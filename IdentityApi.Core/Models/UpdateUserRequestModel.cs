﻿
namespace IdentityApi.Core.Models
{
    public sealed class UpdateUserRequestModel
    {
        public string? UserName { get; set; }
        public string? UserEmail { get; set; }
    }
}
