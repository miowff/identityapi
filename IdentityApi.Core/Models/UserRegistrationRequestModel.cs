﻿namespace IdentityApi.Core.Models
{
    public sealed class UserRegistrationRequestModel
    {
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
