﻿

namespace IdentityApi.Core.Models
{
    public sealed class ChangePasswordRequestModel
    {
        public string? UserEmail { get; set; }
        public string? oldPassworrd { get; set; }
        public string? newPassworrd { get; set; }
    }
}
