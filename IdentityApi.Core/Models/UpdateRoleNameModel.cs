﻿

using System.ComponentModel.DataAnnotations;

namespace IdentityApi.Core.Models
{
    public sealed class UpdateRoleNameModel
    {
        [Required]
        public string? OldName { get; set; }
        [Required]
        public string? NewName { get; set; }
    }
}
