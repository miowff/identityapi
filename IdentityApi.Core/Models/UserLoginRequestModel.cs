﻿

namespace IdentityApi.Core.Models
{
    public sealed class UserLoginRequestModel
    {
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
