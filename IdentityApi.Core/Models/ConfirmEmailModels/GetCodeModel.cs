﻿using System.ComponentModel.DataAnnotations;

namespace IdentityApi.Core.Models.ConfirmEmailModels
{
    public class GetCodeModel
    {
        [Required]
        public string? identityUserId { get;set; }
    }
}
