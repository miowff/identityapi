﻿using System.ComponentModel.DataAnnotations;

namespace IdentityApi.Core.Models.ConfirmEmailModels
{
    public sealed class ConfirmEmailModel
    {
        [Required]
        public string? identityUserId { get; set; }
        [Required]
        public string? code { get; set; }
    }
}
