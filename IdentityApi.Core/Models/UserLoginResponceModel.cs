﻿namespace IdentityApi.Core.Models
{
    public sealed class UserLoginResponceModel
    {
        public string token { get; set; }
        public string RefreshToken { get; set; }
        public UserLoginResponceModel(string token,string refreshToken)
        {
            this.token = "Bearer " + token;
            this.RefreshToken = refreshToken;
        }
    }
}
