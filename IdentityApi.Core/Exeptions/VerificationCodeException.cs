﻿
namespace IdentityApi.Core.Exeptions
{
    public sealed class VerificationCodeException : Exception
    {
        public VerificationCodeException(string message)
       : base(message) { }
    }
}
