﻿
namespace IdentityApi.Core.Exeptions
{
    public sealed class TokenException : Exception
    {
        public TokenException(string message)
        : base(message) { }
    }
}
