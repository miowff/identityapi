﻿

namespace IdentityApi.Core.Exeptions
{
    public sealed class EmailException : Exception
    {
        public EmailException(string message)
        : base(message) { }
    }
}
