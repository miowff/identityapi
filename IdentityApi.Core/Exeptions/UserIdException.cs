﻿

namespace IdentityApi.Core.Exeptions
{
    public sealed class UserIdException : Exception
    {
        public UserIdException(string message)
       : base(message) { }
    }
}
