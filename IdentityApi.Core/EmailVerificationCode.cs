﻿

using Microsoft.AspNetCore.Identity;

namespace IdentityApi.Core
{
    public sealed class EmailVerificationCode
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string? Email { get; set; }
        public string? UserId { get; set; }
        public IdentityUser? identityUser { get; set; }
        public int? VerificationCode { get; set; }
        public int Attempts { get; set; } = 3;
    }
}
