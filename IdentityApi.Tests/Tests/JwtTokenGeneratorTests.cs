﻿using IdentityApi.Data;
using IdentityApi.Domain;
using IdentityApi.Domain.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Moq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace IdentityApi.Tests.TokenGeneratorTests
{
    public sealed class JwtTokenGeneratorTests
    {
        private readonly Mock<IOptionsMonitor<JwtConfig>> _optionsMonitorMoq = new Mock<IOptionsMonitor<JwtConfig>>();
        private ApplicationDbContext _applicationDBContext;

        private JwtTokenService _jwtTokenGeneratorService;

        private readonly JwtConfig _jwtConfig = new JwtConfig() { Secret = "qsgdvasfjbasdfkhasdbaskjfkaf" };

        private readonly List<Claim> _claims = new List<Claim>() {
               new Claim("Id","5387e748-a89b-4ca6-a346-a6fc227546a3"),
               new Claim(JwtRegisteredClaimNames.Sub,"Test@Test"),
               new Claim(JwtRegisteredClaimNames.Email,"Test@Test"),
               new Claim(JwtRegisteredClaimNames.Jti,"5387e748-a89b-4ca6-a346-a6fc227546a2"),
               new Claim(JwtRegisteredClaimNames.UniqueName,"Test"),
               new Claim(ClaimTypes.Role, "User")
            };
        private byte[]? _key;

        private TokenValidationParameters _validationParameters;
        [SetUp]
        public void Setup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "IdentityAptTestDb");
            _applicationDBContext = new ApplicationDbContext(optionsBuilder.Options);
            this._key = Encoding.ASCII.GetBytes(this._jwtConfig.Secret);
            this._validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(this._key),
                ValidateIssuer = false,
                ValidateAudience = false,
                RequireExpirationTime = false,
                ValidateLifetime = true
            };
        }
        [Test]
        public async Task IsResultIsNotNull()
        {
            //Arrange
            _optionsMonitorMoq.Setup(x => x.CurrentValue).Returns(this._jwtConfig);
            this._jwtTokenGeneratorService = new JwtTokenService(_optionsMonitorMoq.Object,
                                                                   _applicationDBContext,
                                                                   _validationParameters);

            //Act
            var result = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            //Assert
            Assert.IsNotNull(result);
        }
        [Test]
        public async Task IsResultTokenIsJwtToken()
        {
            //Arrange
            _optionsMonitorMoq.Setup(x => x.CurrentValue).Returns(this._jwtConfig);
            this._jwtTokenGeneratorService = new JwtTokenService(_optionsMonitorMoq.Object,
                                                                   _applicationDBContext,
                                                                   _validationParameters);

            //Act
            var result = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var token = result.token.Replace("Bearer", "").Replace(" ", "");
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            jwtTokenHandler.ValidateToken(token, this._validationParameters, out var validatedToken);
            //Assert
            Assert.IsTrue(validatedToken is JwtSecurityToken jwtSecurityToken);
        }
        [TestCase("Test@Test", "email")]
        [TestCase("Test@Test", "sub")]
        [TestCase("5387e748-a89b-4ca6-a346-a6fc227546a2", "jti")]
        [TestCase("5387e748-a89b-4ca6-a346-a6fc227546a3", "Id")]
        [TestCase("Test", "unique_name")]
        [TestCase("User", "role")]
        public async Task IsTokenContainsCorrectClaim(string expetedClaim, string itemToCheck)
        {
            //Arrange
            _optionsMonitorMoq.Setup(x => x.CurrentValue).Returns(this._jwtConfig);
            this._jwtTokenGeneratorService = new JwtTokenService(_optionsMonitorMoq.Object,
                                                                   _applicationDBContext,
                                                                   _validationParameters);
            var tokenHandler = new JwtSecurityTokenHandler();
            //Act
            var response = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var token = response.token.Replace("Bearer", "").Replace(" ", "");

            tokenHandler.ValidateToken(token, this._validationParameters, out var validatedToken);
            var jwtToken = (JwtSecurityToken)validatedToken;
            var result = jwtToken.Claims.First(x => x.Type == itemToCheck).Value;
            //Assert
            Assert.IsNotNull(result);
            Assert.That(result, Is.EqualTo(expetedClaim));
        }
        [Test]
        public async Task IsRefreshTokenJtiIdEqalsToJwtTokenJti()
        {
            //Arrange
            _optionsMonitorMoq.Setup(x => x.CurrentValue).Returns(this._jwtConfig);
            this._jwtTokenGeneratorService = new JwtTokenService(_optionsMonitorMoq.Object,
                                                                   _applicationDBContext,
                                                                   _validationParameters);
            var tokenHandler = new JwtSecurityTokenHandler();
            //Act
            var response = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var token = response.token.Replace("Bearer", "").Replace(" ", "");
            tokenHandler.ValidateToken(token, this._validationParameters, out var validatedToken);
            var jwtToken = (JwtSecurityToken)validatedToken;
            var jwtTokenJti = jwtToken.Claims.First(x => x.Type == "jti").Value;
            var refreshTokenFromDb = _applicationDBContext.RefreshTokens
                                                          .FirstOrDefault(token => token.UserId == "5387e748-a89b-4ca6-a346-a6fc227546a3");
            Assert.IsNotNull(refreshTokenFromDb);
            Assert.That(jwtTokenJti, Is.EqualTo(refreshTokenFromDb.JwtId));
        }
        [Test]
        public async Task IsUsedRefreshTokenRemovesFromDb()
        {
            //Arrange
            _optionsMonitorMoq.Setup(x => x.CurrentValue).Returns(this._jwtConfig);
            this._jwtTokenGeneratorService = new JwtTokenService(_optionsMonitorMoq.Object,
                                                                   _applicationDBContext,
                                                                   _validationParameters);
            //Act
            var response = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var refreshTokenFromDb = _applicationDBContext.RefreshTokens
                                                         .First(token => token.UserId == "5387e748-a89b-4ca6-a346-a6fc227546a3");
            refreshTokenFromDb.IsUsed = true;
            _applicationDBContext.Update(refreshTokenFromDb);
            var newResponse = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var newRefreshTokenFromDb= _applicationDBContext.RefreshTokens
                                                         .First(token => token.UserId == "5387e748-a89b-4ca6-a346-a6fc227546a3");
            //Assert
            Assert.That(newRefreshTokenFromDb, Is.Not.EqualTo(refreshTokenFromDb));
        }
        [Test]
        public async Task IsReturnsUnusedRefreshToken()
        {
            //Arrange
            _optionsMonitorMoq.Setup(x => x.CurrentValue).Returns(this._jwtConfig);
            this._jwtTokenGeneratorService = new JwtTokenService(_optionsMonitorMoq.Object,
                                                                   _applicationDBContext,
                                                                   _validationParameters);
            //Act
            var response = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var refreshTokenFromDb = _applicationDBContext.RefreshTokens
                                                         .First(token => token.UserId == "5387e748-a89b-4ca6-a346-a6fc227546a3");
            var newResponse = await this._jwtTokenGeneratorService.GenerateToken(_claims);
            var newRefreshTokenFromDb = _applicationDBContext.RefreshTokens
                                                        .First(token => token.UserId == "5387e748-a89b-4ca6-a346-a6fc227546a3");
            //Assert
            Assert.That(newRefreshTokenFromDb,Is.EqualTo(refreshTokenFromDb));

        }
    }
}
