﻿namespace IdentityApi.Domain
{
    public sealed class JwtConfig
    {
        public string? Secret { get; set; }
    }
}
