﻿
using IdentityApi.Core.Exeptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IdentityApi.Domain.Services
{
    public class DeleteUnconfirmedUsersService : BackgroundService
    {
        private static List<string> _newUsersIds = new List<string>();
        private readonly PeriodicTimer _timer = new(TimeSpan.FromHours(24));
        private readonly IServiceProvider _serviceProvider;
        public DeleteUnconfirmedUsersService(IServiceProvider serviceProvider)
        {
            this._serviceProvider = serviceProvider;
        }
        public static void AddNewUserId(string identityUserId)
        {
            if (!Guid.TryParse(identityUserId, out Guid userId))
            {
                throw new UserIdException($"{identityUserId} is not Guid value");
            }
            _newUsersIds.Add(identityUserId);
        }

        public static void RemoveUserId(string identityUserId)
        {
            if (!Guid.TryParse(identityUserId, out Guid userId))
            {
                throw new UserIdException($"{identityUserId} is not Guid value");
            }
            _newUsersIds.Remove(identityUserId);
        }

        private async Task CheckUsers()
        {
            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                UserManager<IdentityUser> userManager =
                    scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();

                foreach (var userId in _newUsersIds)
                {
                    var existingUser = await userManager.FindByIdAsync(userId);
                    if(existingUser != null && !existingUser.EmailConfirmed)
                    {
                        await userManager.DeleteAsync(existingUser);
                    }
                }
            }
            _newUsersIds.Clear();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (await _timer.WaitForNextTickAsync(stoppingToken) && !stoppingToken.IsCancellationRequested)
            {
                await CheckUsers();
            }
        }
    }
}
