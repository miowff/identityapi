﻿using IdentityApi.Core;
using IdentityApi.Core.Exeptions;
using IdentityApi.Core.ServicesInterfaces;
using IdentityApi.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RestSharp;
using RestSharp.Authenticators;

namespace IdentityApi.Domain.Services
{
    public sealed class EmailService : IEmailService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        public EmailService(UserManager<IdentityUser> userManager, ApplicationDbContext dbContext)
        {
            this._userManager = userManager;
            this._applicationDbContext = dbContext;
        }
        private bool SendEmail(string body, string email)
        {

            RestClient client = new RestClient("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", "c037419b9639d7ec0eac45c4d0c1962d-835621cf-9dacdf67");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "sandbox2f5f8ee20d75403493198fb13530ca9b.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Email verification <mailgun@sandbox2f5f8ee20d75403493198fb13530ca9b.mailgun.org>");
            request.AddParameter("to", email);
            request.AddParameter("to", "sandbox2f5f8ee20d75403493198fb13530ca9b.mailgun.org");
            request.AddParameter("subject", "Confirm your email");
            request.AddParameter("text", body);
            request.Method = Method.Post;
            return client.Execute(request).IsSuccessful;
        }
        public async Task<bool> SendConfirmationCode(string identityUserId)
        {
            var existingUser = await _userManager.FindByIdAsync(identityUserId);
            if (existingUser == null)
            {
                throw new ArgumentNullException(nameof(identityUserId));
            }
            if (existingUser.EmailConfirmed)
            {
                throw new EmailException($"{existingUser.Email} is already confirmed");
            }
            var random = new Random();
            var code = random.Next(100000, 999999);
            var emailVerification = new EmailVerificationCode()
            {
                Email = existingUser.Email,
                identityUser = existingUser,
                UserId = existingUser.Id,
                VerificationCode = code
            };
            await _applicationDbContext.EmailVerificationCodes.AddAsync(emailVerification);
            var result = this.SendEmail($"Your code {code}", existingUser.Email);
            await _applicationDbContext.SaveChangesAsync();
            return result;
        }

        public async Task<bool> ConfirmEmail(string identityUserId, string code)
        {
            var existingUser = await _userManager.FindByIdAsync(identityUserId);
            if (existingUser == null)
            {
                throw new ArgumentNullException(nameof(identityUserId));
            }
            if (existingUser.EmailConfirmed)
            {
                throw new EmailException($"{existingUser.Email} is already confirmed");
            }
            var verificationCode = await _applicationDbContext.EmailVerificationCodes
                                           .FirstOrDefaultAsync(code => code.UserId == existingUser.Id);
            if (verificationCode == null)
            {
                throw new ArgumentNullException(nameof(identityUserId));
            }
            if (verificationCode.Attempts == 0)
            {
                _applicationDbContext.EmailVerificationCodes.Remove(verificationCode);
                _applicationDbContext.SaveChanges();
                throw new VerificationCodeException("Out of attempts call confirmation email again");
            }

            int.TryParse(code, out int intCode);

            if (verificationCode.VerificationCode != intCode)
            {
                verificationCode.Attempts--;
                _applicationDbContext.EmailVerificationCodes.Update(verificationCode);
                _applicationDbContext.SaveChanges();
                throw new EmailException($"{code} is wrong");
            }

            _applicationDbContext.EmailVerificationCodes.Remove(verificationCode);
            _applicationDbContext.SaveChanges();
            existingUser.EmailConfirmed = true;
            await _userManager.UpdateAsync(existingUser);

            DeleteUnconfirmedUsersService.RemoveUserId(existingUser.Id);

            return true;
        }
    }
}
