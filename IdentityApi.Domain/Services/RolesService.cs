﻿
using IdentityApi.Core.Models;
using IdentityApi.Core.ServicesInterfaces;
using Microsoft.AspNetCore.Identity;

namespace IdentityApi.Domain.Services
{
    public sealed class RolesService:IRolesService
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        public RolesService(RoleManager<IdentityRole> roleManager)
        {
            this._roleManager = roleManager;
        }

        public async Task CreateRole(string roleName)
        {
            var existingRole = await _roleManager.FindByNameAsync(roleName);
            if (existingRole != null)
            {
                throw new ArgumentException(roleName);
            }
            await _roleManager.CreateAsync(new IdentityRole(roleName));
        }

        public async Task DeleteRole(string roleName)
        {
            var existingRrole = await _roleManager.FindByNameAsync(roleName);
            if (existingRrole == null)
            {
                throw new ArgumentNullException(roleName);
            }
            await _roleManager.DeleteAsync(existingRrole);
        }

        public async Task UpdateRoleName(UpdateRoleNameModel model)
        {
            var existingRole = await _roleManager.FindByNameAsync(model.OldName);
            if (existingRole == null)
            {
                throw new ArgumentNullException(model.OldName);
            }
            existingRole.Name = model.NewName;
            await _roleManager.UpdateAsync(existingRole);
        }
    }
}
