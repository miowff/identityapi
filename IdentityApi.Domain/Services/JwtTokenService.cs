﻿using IdentityApi.Core;
using IdentityApi.Core.Exeptions;
using IdentityApi.Core.Models;
using IdentityApi.Core.ServicesInterfaces;
using IdentityApi.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace IdentityApi.Domain.Services
{
    public sealed class JwtTokenService : IJwtTokenService
    {
        private readonly JwtConfig _jwtConfig;
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly TokenValidationParameters _tokenValidationParams;

        public JwtTokenService(IOptionsMonitor<JwtConfig> optionMonitor,
            ApplicationDbContext applicationDbContext,
            TokenValidationParameters tokenValidationParams)
        {
            this._jwtConfig = optionMonitor.CurrentValue;
            this._applicationDbContext = applicationDbContext;
            this._tokenValidationParams = tokenValidationParams;
        }
        /// <summary>
        /// Generates a random string for refreshToken
        /// </summary>
        private string RandomString(int length)
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
            return new string(Enumerable.Repeat(chars, length).Select(x => x[random.Next(x.Length)]).ToArray());
        }
        /// <summary>
        /// Acess+refresh tokens generation logic adn saving refresh token into DB
        /// Calls by <class cref="UsersService">
        /// Lifetime of RefreshToken is one month
        /// lifetime of JwtToken is 5 minutes
        /// </summary>

        public async Task<UserLoginResponceModel> GenerateToken(List<Claim> claims)
        {
             var jwtHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddMinutes(5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = jwtHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtHandler.WriteToken(token);
            var userId = claims.FirstOrDefault(claim => claim.Type == "Id").Value;
            var existingRefreshToken = await _applicationDbContext.RefreshTokens.FirstOrDefaultAsync(token => token.UserId == userId);

            if (existingRefreshToken != null && DateTime.Now < existingRefreshToken.ExpiryDate && !existingRefreshToken.IsUsed)
            {
                existingRefreshToken.JwtId = token.Id;
                _applicationDbContext.RefreshTokens.Update(existingRefreshToken);
                await _applicationDbContext.SaveChangesAsync();
                return new UserLoginResponceModel(jwtToken, existingRefreshToken.Token);
            }

            if (existingRefreshToken != null)
            {
                _applicationDbContext.Remove(existingRefreshToken);
            }

            var refreshToken = new RefreshToken()
            {
                JwtId = token.Id,
                IsUsed = false,
                IsRevorked = false,
                UserId = userId,
                Created = DateTime.Now,
                ExpiryDate = DateTime.Now.AddMonths(1),
                Token = RandomString(35) + Guid.NewGuid()
            };
            await _applicationDbContext.RefreshTokens.AddAsync(refreshToken);
            await _applicationDbContext.SaveChangesAsync();
            return new UserLoginResponceModel(jwtToken, refreshToken.Token);
        }

        public ClaimsPrincipal GetClaimsPrincipalToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenInVerification = tokenHandler.ValidateToken(token, this._tokenValidationParams, out var validatedToken);
            if (validatedToken is JwtSecurityToken jwtSecurityToken)
            {
                var result = jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase);
                if (!result)
                {
                    throw new TokenException("Wrong jwt header");
                }
            }
            return tokenInVerification;
        }
    }
}
