﻿using Microsoft.AspNetCore.Identity;

namespace IdentityApi.Domain.Services
{
    public static class UsersAndRolesInitializer
    {
        public static async Task InitializeAsync(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            if (await roleManager.FindByNameAsync("Admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("Admin"));
            }
            if (await roleManager.FindByNameAsync("User") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("User"));
            }
            if (await userManager.FindByEmailAsync("Admin@Admin") == null)
            {
                var user = new IdentityUser { UserName = "Admin", Email = "Admin@Admin", EmailConfirmed = true };
                await userManager.CreateAsync(user, "Admin123!!!");
                await userManager.AddToRoleAsync(user, "Admin");
                await userManager.AddToRoleAsync(user, "User");
            }
        }
    }
}
