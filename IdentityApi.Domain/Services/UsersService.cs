﻿using IdentityApi.Core.Exeptions;
using IdentityApi.Core.Models;
using IdentityApi.Core.ServicesInterfaces;
using IdentityApi.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace IdentityApi.Domain.Services
{
    public sealed class UsersService : IUsersService
    {
        private readonly IJwtTokenService _jwtTokenService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;
        public UsersService(IJwtTokenService jwtTokenService,
            UserManager<IdentityUser> userManager,
            ApplicationDbContext applicationDbContext)
        {
            this._jwtTokenService = jwtTokenService;
            this._userManager = userManager;
            this._applicationDbContext = applicationDbContext;
        }
        private async Task<List<Claim>> GetUserCalims(IdentityUser user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);
            List<Claim> userClaims = new List<Claim>()
            {
               new Claim("Id",user.Id),
               new Claim(JwtRegisteredClaimNames.Sub,user.Email),
               new Claim(JwtRegisteredClaimNames.Email,user.Email),
               new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
               new Claim(JwtRegisteredClaimNames.UniqueName,user.UserName)
            };
            foreach (var role in userRoles)
            {
                userClaims.Add(new Claim(ClaimTypes.Role, role));
            }
            return userClaims;
        }

        /// <summary>
        /// Converts Jwt token expiry date from unix time stamo to DateTime
        /// Pay attention: Local date time used for tokens lifetime generation
        /// </summary>
        private DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            var dateTimeValue = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified);
            dateTimeValue = dateTimeValue.AddSeconds(unixTimeStamp);
            return dateTimeValue.AddHours(3);
        }
        public async Task ChangePasswordAsync(ChangePasswordRequestModel changePasswordModel)
        {
            var existedUser = await _userManager.FindByEmailAsync(changePasswordModel.UserEmail);
            if (existedUser == null)
            {
                throw new ArgumentNullException(changePasswordModel.UserEmail);
            }
            await _userManager.ChangePasswordAsync(existedUser, changePasswordModel.oldPassworrd, changePasswordModel.newPassworrd);
        }

        public async Task DeleteAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            await _userManager.DeleteAsync(user);
        }

        public async Task<UserLoginResponceModel> Login(UserLoginRequestModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                throw new ArgumentNullException(model.Email);
            }
            var isPasswordCorrect = await _userManager.CheckPasswordAsync(user, model.Password);
            if (!isPasswordCorrect)
            {
                throw new ArgumentException(model.Password);
            }

            return await _jwtTokenService.GenerateToken(await GetUserCalims(user));
        }

        public async Task Register(UserRegistrationRequestModel model)
        {
            var existingUser = await _userManager.FindByEmailAsync(model.Email);
            if (existingUser != null)
            {
                throw new ArgumentException(model.Email);
            }
            var newUser = new IdentityUser()
            {
                Email = model.Email,
                UserName = model.Name,
                EmailConfirmed = false
            };
            var isCreated = await _userManager.CreateAsync(newUser, model.Password);
            if (!isCreated.Succeeded)
            {
                throw new ArgumentException(model.Email);
            }
            await _userManager.AddToRoleAsync(newUser, "User");

            DeleteUnconfirmedUsersService.AddNewUserId(newUser.Id);
        }

        public async Task UpdateAsync(UpdateUserRequestModel model, string oldEmail)
        {
            var user = await _userManager.FindByEmailAsync(oldEmail);
            if (user == null)
            {
                throw new ArgumentNullException(oldEmail);
            }
            user.Email = model.UserEmail;
            user.UserName = model.UserName;
            await _userManager.UpdateAsync(user);
        }
      
        public async Task<UserLoginResponceModel> RefreshToken(TokenRequest tokenRequest)
        {

            var token = tokenRequest.Token.Replace("Bearer", "").Replace(" ", "");
            var tokenInVerification = this._jwtTokenService.GetClaimsPrincipalToken(token);

            var utcExpiryDate = long.Parse(tokenInVerification.Claims.FirstOrDefault(claim => claim.Type == JwtRegisteredClaimNames.Exp).Value);
            var expiryDate = UnixTimeStampToDateTime(utcExpiryDate);

            //DateTime Validation
            if (expiryDate > DateTime.Now)
            {
                throw new TokenException("Token not expired yet");
            }
            var refreshToken = await _applicationDbContext.RefreshTokens.FirstOrDefaultAsync(x => x.Token == tokenRequest.RefreshToken);
            if (refreshToken == null)
            {
                throw new TokenException("Refresh token is null");
            }

            if (refreshToken.IsUsed)
            {
                throw new TokenException("Refresh token is used");
            }
            if (refreshToken.IsRevorked)
            {
                throw new TokenException("Refresh token is revoked");
            }
            var tokenId = tokenInVerification.Claims.FirstOrDefault(claim => claim.Type == JwtRegisteredClaimNames.Jti).Value;
            if (refreshToken.JwtId != tokenId)
            {
                throw new TokenException("Refresh token is not belongs to jwt token");
            }
            refreshToken.IsUsed = true;
            _applicationDbContext.RefreshTokens.Update(refreshToken);
            await _applicationDbContext.SaveChangesAsync();
            var user = await _userManager.FindByIdAsync(refreshToken.UserId);
            var responce = await this._jwtTokenService.GenerateToken(await GetUserCalims(user));
            return responce;
        }

    }
}

